import React from "react";
import { TopBar, TableHead, TableBody, Table } from "./table.js";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Table>
        <TopBar />
        <TableHead />
        <TableBody />
      </Table>
    </div>
  );
}

export default App;
