import React from "react";

const TopBar = () => {
  return <h1> Witam </h1>;
};

const TableHead = () => {
  return (
    <tr>
      <th></th>
      <th>Napastnik</th>
      <th>Pomocnik</th>
      <th>Obrońca</th>
    </tr>
  );
};

const TableBody = () => {
  return (
    <tbody>
      <tr>
        <th>Imię</th>
        <td>Jan </td>
        <td>Tomasz </td>
        <td>Albin </td>
      </tr>
      <tr>
        <th>Nazwisko</th>
        <td>Kolano </td>
        <td>Wątroba </td>
        <td>Złeczoło </td>
      </tr>
    </tbody>
  );
};

const Table = props => {
  return <table>{props.children}</table>;
};

export { TopBar };
export { TableHead };
export { TableBody };
export { Table };
